<?php

namespace Missbach\ProcessBundle\Core\Interfaces;

/**
 * Interface IResolvable
 * @package Missbach\ProcessBundle\Interfaces
 */
interface IResolvable
{
    /**
     * @return mixed
     */
    public function __beforeStart();

    /**
     * @return []
     */
    public function __getDebugMessages();

    /**
     * @param $slot
     * @param $referenceId
     * @param $reverseSlot
     * @return void
     */
    public function __setInputIdForSlot($slot,$referenceId,$reverseSlot);

    /**
     * @param $slot
     * @param $referenceId
     * @param $reverseSlot
     * @return void
     */
    public function __setOutputIdForSlot($slot,$referenceId,$reverseSlot);

    /**
     * @param array $elems
     * @return void
     */
    public function __setInputIds($elems);

    /**
     * @param array $elems
     * @return void
     */
    public function __setOutputIds($elems);

    /**
     * @return mixed
     */
    public function __getInputIds();

    /**
     * @return mixed
     */
    public function __getOutputIds();

    /**
     * @return mixed
     */
    public function __getConfig();

    /**
     * @return mixed
     */
    public function __setConfig($config);

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode);

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode);

    /**
     * @param bool $dryMode
     */
    public function __execute($dryMode);

    /**
    * @param $channel
    * @param $result
    * @return void
    */
    public function __setResult($channel,$result);

    /**
     * @return array
     */
    public function __getRawResults();

    /**
     * @return int
     */
    public function __getActiveOutput();

    /**
     * @param ITreeContext $context
     * @return void
     */
    public function __setContext(ITreeContext $context);

    /**
     * @return ITreeContext
     */
    public function __getContext();

    /**
     * @return bool
     */
    public function __isNoParentCheck();

    /**
     * @return bool
     */
    public function __forceContinueProcess();
}
