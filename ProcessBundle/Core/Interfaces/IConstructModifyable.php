<?php

namespace Missbach\ProcessBundle\Core\Interfaces;

/**
 * Class IConstructModifyable
 * @package Missbach\ProcessBundle\Core\Interfaces
 */
interface IConstructModifyable
{
    /**
     * @param IConnectable $instance
     * @param $id
     * @param $processMetas
     * @return mixed
     */
    public function __onConstruct(IConnectable $instance,&$id, &$processMetas);

    /**
     * @param IConnectable $instance
     * @return mixed
     */
    public function __onDestruct(IConnectable $instance);
}