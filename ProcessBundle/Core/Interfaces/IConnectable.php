<?php

namespace Missbach\ProcessBundle\Core\Interfaces;

/**
 * Interface IConnectable
 * @package Missbach\ProcessBundle\Interfaces
 */
interface IConnectable
{
    /**
     * @param $key
     * @return mixed
     */
    public function __getProcessMeta($key);

    /**
     * @param $key
     * @param $value
     * @return void
     */
    public function __setProcessMeta($key,$value);

    /**
     * @return array
     */
    public function __getProcessInformations();

    /**
     * @param array $_process
     * @return void
     */
    public function __setProcessInformations($_process);
}
