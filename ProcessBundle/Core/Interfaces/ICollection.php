<?php

namespace Missbach\ProcessBundle\Core\Interfaces;

/**
 * Interface ICollection
 * @package Missbach\ProcessBundle\Core\Interfaces
 */
interface ICollection
{
    /**
     * @param $key
     * @param $item
     * @return void
     */
    public function addItem($key,$item);

    /**
     * @param $key
     * @return void
     */
    public function removeItem($key);

    /**
     * @param $key
     * @return mixed
     */
    public function getItem($key);

    /**
     * @return array
     */
    public function getItems();

    /**
     * @param $key
     * @return bool
     */
    public function hasItem($key);

    /**
     * @return int
     */
    public function count();
}
