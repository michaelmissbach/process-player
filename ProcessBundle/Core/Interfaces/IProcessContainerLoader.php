<?php

namespace Missbach\ProcessBundle\Core\Interfaces;

use Missbach\ProcessBundle\Core\ProcessStructureTree\Element;

/**
 * Interface IProcessContainerLoader
 * @package Missbach\ProcessBundle\Core\Interfaces
 */
interface IProcessContainerLoader
{
    /**
     * @return void
     */
    public function load();

    /**
     * @param Element $root
     * @return void
     */
    public function save(Element $root);

    /**
     * @return array
     */
    public function getFlat();

    /**
     * @return array
     */
    public function getListWithMetas();

    /**
     * @return mixed
     */
    public function getTreeStructure();

    /**
     * @param $id
     * @return ???
     */
    public function getProcessById($id);

    /**
     * @param $id
     * @param ??? $process
     * @return mixed
     */
    public function setProcessById($id, $process);
}