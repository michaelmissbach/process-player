<?php

namespace Missbach\ProcessBundle\Core\Interfaces;

use Missbach\ProcessBundle\Core\Collections\ContextCollection;

/**
 * Interface ITreeContext
 * @package Missbach\ProcessBundle\Core\Interfaces
 */
interface ITreeContext
{
    /**
     * @return ContextCollection
     */
    public function getParameterCollection();

    /**
     * @param ContextCollection $parameterCollection
     * @return AbstractTreeContext
     */
    public function setParameterCollection(ContextCollection $parameterCollection);
}