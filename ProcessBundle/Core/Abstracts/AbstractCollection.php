<?php

namespace Missbach\ProcessBundle\Core\Abstracts;

use Missbach\ProcessBundle\Core\Interfaces\ICollection;

/**
 * Class AbstractCollection
 * @package Missbach\ProcessBundle\Core\Abstracts
 */
abstract class AbstractCollection implements ICollection
{
    /**
     * @var array
     */
    protected $collection = [];

    /**
     * @var bool
     */
    protected $dirty = false;

    /**
     * @return bool
     */
    public function isDirty()
    {
        return $this->dirty;
    }

    /**
     *
     */
    public function clearDirty()
    {
        $this->dirty = false;
    }

    /**
     * @return void
     */
    public function setDirty()
    {
        $this->dirty = true;
    }

    /**
     * @param $key
     * @param $item
     */
    public function addItem($key,$item)
    {
        $this->dirty = true;
        $this->collection[$key] = $item;
    }

    /**
     * @param $key
     */
    public function removeItem($key)
    {
        $this->dirty = true;
        unset($this->collection[$key]);
    }

    /**
     * @param $key
     */
    public function removeItems()
    {
        $this->dirty = true;
        $this->collection = [];
    }

    /**
     * @param $key
     */
    public function getItem($key)
    {
        return $this->collection[$key];
    }

    /**
     *
     */
    public function getItems()
    {
        return $this->collection;
    }

    /**
     * @param $key
     * @return bool
     */
    public function hasItem($key)
    {
        return array_key_exists($key,$this->collection);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->collection);
    }
}
