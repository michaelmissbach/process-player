<?php

namespace Missbach\ProcessBundle\Core\Abstracts;

use Missbach\ProcessBundle\Core\Collections\ContextCollection;
use Missbach\ProcessBundle\Core\Interfaces\ITreeContext;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class AbstractTreeContext
 * @package Missbach\ProcessBundle\Core\Abstracts
 */
class AbstractTreeContext implements ITreeContext
{
    /**
     * @var ContextCollection
     */
    protected $parameterCollection;

    /**
     * Context constructor.
     */
    public function __construct()
    {
        $this->parameterCollection = new ContextCollection();
    }

    /**
     * @return ContextCollection
     */
    public function getParameterCollection()
    {
        return $this->parameterCollection;
    }

    /**
     * @param ContextCollection $parameterCollection
     * @return AbstractTreeContext
     */
    public function setParameterCollection(ContextCollection $parameterCollection)
    {
        $this->parameterCollection = $parameterCollection;
        return $this;
    }
}
