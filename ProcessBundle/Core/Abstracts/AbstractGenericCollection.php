<?php

namespace Missbach\ProcessBundle\Core\Abstracts;

use Missbach\ProcessBundle\Core\Exceptions\NotAllowedTypeException;

/**
 * Class AbstractCollection
 * @package Missbach\ProcessBundle\Core\Abstracts
 */
abstract class AbstractGenericCollection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $type;

    /**
     * AbstractGenericCollection constructor.
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @param $key
     * @param $item
     */
    public function addItem($key,$item)
    {
        if ($this->__isType($item)){
            parent::addItem($key,$item);
            return;
        }
        throw new NotAllowedTypeException();
    }

    /**
     * @param $item
     * @return bool
     */
    final private function __isType($item)
    {
        return $item instanceof $this->type;
    }
}
