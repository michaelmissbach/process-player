<?php

namespace Missbach\ProcessBundle\Core\ExecutedTree;

use Missbach\ProcessBundle\Core\Exceptions\NoEntryFoundExeption;
use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\ITreeContext;
use Missbach\ProcessBundle\Modules\Start\StartConnectable;

/**
 * Class Builder
 * @package Missbach\ProcessBundle\Tree
 */
class Builder
{
    /**
     * @var array
     */
    protected static $trace = [];

    /**
     * @var bool
     */
    protected static $result = false;

    /**
     * @var array
     */
    protected static $finishedIds = [];

    /**
     * @var bool
     */
    protected static $dryMode = false;

    /**
     * @var ITreeContext
     */
    protected static $context = null;

    /**
     * @return bool
     */
    public static function isDryMode()
    {
        return self::$dryMode;
    }

    /**
     * @param bool $dryMode
     */
    public static function setDryMode($dryMode)
    {
        self::$dryMode = $dryMode;
    }

    /**
     * @param $message
     */
    public static function addTrace($message)
    {
        self::$trace[] = $message;
    }

    /**
     * @return array
     */
    public function getTrace()
    {
        return self::$trace;
    }

    /**
     * @param $result
     */
    public static function setResult($result)
    {
        self::$result = $result;
    }

    /**
     * @return bool
     */
    public function getResult()
    {
        return self::$result;
    }

    /**
     * @param $id
     */
    public static function addFinishedId($id)
    {
        if (!isset(self::$finishedIds[$id])) {
            self::$finishedIds[$id] = 0;
        }
        self::$finishedIds[$id]++;
    }

    /**
     * @param $id
     */
    public function getFinishedId()
    {
        return self::$finishedIds;
    }

    /**
     * @return ITreeContext
     */
    public static function getContext()
    {
        return self::$context;
    }

    /**
     * @param ITreeContext $context
     */
    public static function setContext(ITreeContext $context)
    {
        self::$context = $context;
    }

    /**
     * @return ITreeContext
     */
    public function getContextFromInstance()
    {
        return self::$context;
    }

    /**
     * @param $id
     * @return bool
     */
    public static function isCircular($id)
    {
        return isset(self::$finishedIds[$id]) && is_int(self::$finishedIds[$id]) && self::$finishedIds[$id] > 20;
    }

    /**
     * @var TreeElement
     */
    protected $tree;

    /**
     * @var array
     */
    protected $list;

    /**
     * @param $list
     * @param null $context
     */
    public function createTree($list,$context = null)
    {
        $this->createInitialTreeElements($list);
        $this->setRelations();
        $this->setContextOnInstances($context);
    }

    /**
     * @return Element|null
     */
    public function getStart()
    {
        $start = null;
        /** @var Element $treeElement */
        foreach ($this->list as $key=>$treeElement) {
            if ($treeElement->getNativeElement() instanceof StartConnectable) {
                if ($start) {
                    throw new NoEntryFoundExeption('There are multiple startpoints.');
                }
                $start = $treeElement;
            }
        }
        return $start;
    }

    /**
     * @param null $context
     */
    protected function setContextOnInstances($context = null)
    {
        if (!(self::$context instanceof ITreeContext)) {
            self::$context = new Context();
        } else {
            self::$context = $context;
        }

        /** @var Element $treeElement */
        foreach ($this->list as $key=>$treeElement) {
            $treeElement->setContext(self::$context);
        }
    }

    /**
     * @param $list
     * @return void
     */
    protected function createInitialTreeElements($list)
    {
        $this->list = [];
        foreach ($list as $key=>$nativeElement) {
            $treeElement = new Element();
            $treeElement->setNativeElement($nativeElement);
            $treeElement->setId($key);
            $this->list[$key] = $treeElement;
        }
    }

    /**
     * @return void
     */
    protected function setRelations()
    {
        /** @var Element $treeElement*/
        foreach ($this->list as $currentElementId=>$treeElement) {
            foreach ($treeElement->getInputIds() as $currentElementChannel=>$relatedInformations) {
                foreach ($relatedInformations as $relatedId=>$relatedChannel) {
                    /** @var Element $relatedElement */
                    $relatedElement = $this->list[$relatedId];
                    $treeElement->addParent($currentElementChannel,$relatedChannel,$relatedElement);
                    $relatedElement->addChild($relatedChannel,$currentElementChannel,$treeElement);
                }
            }
            foreach ($treeElement->getOutputIds() as $currentElementChannel=>$relatedInformations) {
                foreach ($relatedInformations as $relatedId=>$relatedChannel) {
                    /** @var Element $relatedElement */
                    $relatedElement = $this->list[$relatedId];
                    $treeElement->addChild($currentElementChannel,$relatedChannel,$relatedElement);
                    $relatedElement->addParent($relatedChannel,$currentElementChannel,$treeElement);
                }
            }
        }
    }
}
