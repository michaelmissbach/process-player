<?php

namespace Missbach\ProcessBundle\Core\ExecutedTree;

use Missbach\ProcessBundle\Core\Abstracts\AbstractTreeContext;
use Missbach\ProcessBundle\Core\Collections\ContextCollection;
use Missbach\ProcessBundle\Core\Interfaces\ITreeContext;

/**
 * Class Context
 * @package Missbach\ProcessBundle\Core\ExecutedTree
 */
class Context extends AbstractTreeContext implements ITreeContext
{
}
