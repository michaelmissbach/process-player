<?php

namespace Missbach\ProcessBundle\Core\ExecutedTree;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Exceptions\CircularProcessObjectException;
use Missbach\ProcessBundle\Core\Interfaces\ITreeContext;

/**
 * Class Element
 * @package Missbach\ProcessBundle\Tree
 */
class Element
{
    /**
     * @var IConnectable
     */
    protected $nativeElement;

    /**
     * @var
     */
    protected $id;

    /**
     * @var array
     */
    protected $parents = [];

    /**
     * @var array
     */
    protected $children = [];

    /**
     * @var array
     */
    protected $messages = [];

    /**
     * @param ITreeContext $context
     * @return void
     */
    public function setContext(ITreeContext $context)
    {
        $this->nativeElement->__setContext($context);
    }

    /**
     * @return IConnectable
     */
    public function getNativeElement()
    {
        return $this->nativeElement;
    }

    /**
     * @param IConnectable $nativeElement
     * @return Element
     */
    public function setNativeElement(IConnectable $nativeElement)
    {
        $this->nativeElement = $nativeElement;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Element
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInputIds()
    {
        return $this->nativeElement->__getInputIds();
    }

    /**
     * @return mixed
     */
    public function getOutputIds()
    {
        return $this->nativeElement->__getOutputIds();
    }

    /**
     * @return int
     */
    public function getActiveOutput()
    {
        return $this->nativeElement->__getActiveOutput();
    }

    /**
     * @return bool
     */
    public function isNoParentCheck()
    {
        return $this->nativeElement->__isNoParentCheck();
    }

    /**
     * @return bool
     */
    public function forceContinueProcess()
    {
        return $this->nativeElement->__forceContinueProcess();
    }

    /**
     * @param $currentChannel
     * @param $relatedChannel
     * @param $relatedElement
     */
    public function addParent($currentChannel,$relatedChannel,$relatedElement)
    {
        $this->parents[$currentChannel] = $relatedElement;
    }

    /**
     * @param $currentChannel
     * @param $relatedChannel
     * @param $relatedElement
     */
    public function addChild($currentChannel,$relatedChannel,$relatedElement)
    {
        $this->children[$currentChannel] = $relatedElement;
    }

    /**
     * @return bool
     */
    public function isResolved()
    {
        try {
            $result = $this->getResult();
        }catch(\Exception $e) {
            $result = false;
        }

        Builder::addTrace(sprintf('Check if %s(%s) is resolved (%s).',$this->id,$this->getName(),$result?'true':'false'));

        Builder::setResult($result);

        return $this->nativeElement->__isResolved(Builder::isDryMode());
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        $objectInformations = $this->nativeElement->__getProcessMeta('objectInformations');
        return  $objectInformations['name'];
    }

    /**
     * @return bool
     */
    public function getResult()
    {
        return $this->nativeElement->__getResult(Builder::isDryMode());
    }

    /**
     * @param bool $comesFromChild
     * @param null $currentParent
     * @throws CircularProcessObjectException
     */
    public function resolve($comesFromChild = false,$currentParent = null)
    {
        if (Builder::isCircular($this->id)) {
           throw new CircularProcessObjectException();
        }

        Builder::addTrace(sprintf('Entered resolve %s(%s).',$this->id,$this->getName()));
        Builder::addFinishedId($this->id);

        $objectInformations = $this->nativeElement->__getProcessMeta('objectInformations');
        if (count($this->parents) && $objectInformations['inputs'] && !$this->isNoParentCheck()) {
            for ($i = 1; $i <= (int)$objectInformations['inputs']; $i++) {
                if (isset($this->parents[$i]) && $this->parents[$i] instanceof Element) {
                    /** @var Element $element */
                    if (!$this->parents[$i]->isResolved()) {
                        $this->parents[$i]->resolve(true,$this);
                    }
                    $this->nativeElement->__setResult($i,$this->parents[$i]->getResult());
                }
            }
        }

        if ($comesFromChild) {
            return;
        }

        $this->nativeElement->__execute(Builder::isDryMode());

        if (($this->isResolved() && $this->getResult()) || $this->forceContinueProcess()) {
            $child = isset($this->children[$this->nativeElement->__getActiveOutput()]) ? $this->children[$this->nativeElement->__getActiveOutput()] : null;

            if ($this->isNoParentCheck()) {
                $result = $currentParent->getResult();
                $this->nativeElement->__setResult(0,$currentParent->getResult());
            }

            if (count($this->nativeElement->__getDebugMessages())) {
                foreach ($this->nativeElement->__getDebugMessages() as $message) {
                    Builder::addTrace($message);
                }
            }

            if ($child instanceof Element) {
                $child->resolve(false,$this);
            } else {
                if ($this->isNoParentCheck()) {
                    Builder::setResult($currentParent->getResult());
                }
            }
        } else {
            if (count($this->nativeElement->__getDebugMessages())) {
                foreach ($this->nativeElement->__getDebugMessages() as $message) {
                    Builder::addTrace($message);
                }
            }
        }
    }
}
