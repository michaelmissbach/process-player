<?php

namespace Missbach\ProcessBundle\Core\ProcessStructureTree;

/**
 * Class Element
 * @package Missbach\ProcessBundle\Core\ProcessStructureTree
 */
class Element
{
    const SORTING_STEP = 500;

    /**
     * @var Element
     */
    protected $parent;

    /**
     * @var array
     */
    protected $children;

    /**
     * @var integer
     */
    protected $sorting;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var string
     */
    protected $shownName;

    /**
     * @var string
     */
    protected $callName;

    /**
     * @var string
     */
    protected $id;

    /**
     * Element constructor.
     */
    public function __construct()
    {
        $this->id = sprintf('t_e_%s',uniqid());
        $this->shownName = $this->id;
        $this->callName = $this->id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Element
     */
    public function setId(string $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Element
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Element $parent
     * @return Element
     */
    public function setParent(Element $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param array $children
     * @return Element
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @param Element $child
     * @param null $key
     * @return $this
     */
    public function addChild(Element $child,$key = null)
    {
        if (!$key) {
            $key = $child->getId();
        }

        if (!$this->hasChildren()) {
            $this->children = [];
        }
        
        $currentSorting = self::SORTING_STEP;
        if (count($this->children)) {
            $lastElement = end($this->children);
            
            $currentSorting = $lastElement->getSorting() + self::SORTING_STEP;
        }
        $child->setSorting($currentSorting);
        $child->setParent($this);
        $this->children[$key] = $child;

        
        return $this;
    }

    /**
     * @param $key
     * @return Element
     */
    public function getChildByKey($key)
    {
        return $this->hasChildren() && array_key_exists($key,$this->children) ? $this->children[$key] : null;
    }

    /**
     * @param Element $element
     */
    public function removeChild(Element $element)
    {
        unset($this->children[$element->getId()]);
        $this->reSortingChildren();
    }

    /**
     * @param $key
     * @return void
     */
    public function setAfterElementWithKey($key)
    {
        if ($this->isRoot()) {
            return;
        }

        $element = $this->parent->getChildByKey($key);
        if ($element instanceof Element) {
            $sorting = $element->getSorting() + (self::SORTING_STEP / 2);
            $this->setSorting($sorting);
            $this->parent->reSortingChildren();
        }
    }

    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     * @return Element
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
        return $this;
    }

    /**
     * @return void
     */
    public function reSortingChildren()
    {
        $tmp = [];

        /** @var Element $child */
        foreach ($this->children as $key=>$child) {
            $sorting = $child->getSorting();
            while(array_key_exists($sorting,$tmp)) {
                $sorting++;
            }
            $tmp[$sorting] = [];
            $tmp[$sorting][$key] = $child;
        }

        ksort($tmp);
        $this->clearChildren();

        foreach ($tmp as $sorting=>$elementPair) {
            $key = key($elementPair);
            $this->addChild($elementPair[$key],$key);
        }
        $tmp = null;
    }

    /**
     * @return void
     */
    public function clearChildren()
    {
        $this->children = null;
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        return $this->parent === null;
    }    

    /**
     * @return bool
     */
    public function isNode()
    {
        return $this->hasChildren();
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return is_array($this->children) && count($this->children);
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return Element
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return string
     */
    public function getShownName()
    {
        return $this->shownName;
    }

    /**
     * @param string $shownName
     * @return Element
     */
    public function setShownName( $shownName)
    {
        $this->shownName = $shownName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCallName()
    {
        return $this->callName;
    }

    /**
     * @param string $callName
     * @return Element
     */
    public function setCallName($callName)
    {
        $this->callName = $callName;
        return $this;
    }
}
