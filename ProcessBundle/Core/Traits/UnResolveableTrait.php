<?php

namespace Missbach\ProcessBundle\Core\Traits;

use Missbach\ProcessBundle\Core\Exceptions\NotImplementedException;
use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\ITreeContext;

/**
 * Trait UnResolveableTrait
 * @package Missbach\ProcessBundle\Traits
 */
trait UnResolveableTrait
{
    use ConnectableTrait;

    /**
     * todo: modify params
     * @var array
     */
    protected $__process = [
        'metas'=>[],
        'inputIds'=>[],
        'outputIds'=>[],
        'results'=>[],
        'config'=>[],
        'context'=>null
    ];
}
