<?php

namespace Missbach\ProcessBundle\Core\Traits;

/**
 * Trait ConnectableTrait
 * @package Missbach\ProcessBundle\Core\Traits
 */
trait ConnectableTrait
{
    /**
     * @return array
     */
    public function __getProcessInformations()
    {
        return $this->__process;
    }

    /**
     * @param array $_process
     * @return void
     */
    public function __setProcessInformations($_process)
    {
        $this->__process = $_process;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __getProcessMeta($key)
    {
        if (array_key_exists($key,$this->__process['metas'])) {
            return $this->__process['metas'][$key];
        }

        return null;
    }

    /**
     * @param $key
     * @param $value
     * @return void
     */
    public function __setProcessMeta($key,$value)
    {
        $this->__process['metas'][$key] = $value;
    }
}