<?php

namespace Missbach\ProcessBundle\Core\Traits;

use Missbach\ProcessBundle\Core\Exceptions\NotImplementedException;
use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\ITreeContext;

/**
 * Trait ResolveableTrait
 * @package Missbach\ProcessBundle\Traits
 */
trait ResolveableTrait
{
    use ConnectableTrait;

    /**
     * @var array
     */
    protected $__process = [
        'metas'=>[],
        'inputIds'=>[],
        'outputIds'=>[],
        'results'=>[],
        'config'=>[],
        'context'=>null
    ];

    /**
     * @param $slot
     * @param $referenceId
     * @param $reverseSlot
     * @return void
     */
    public function __setInputIdForSlot($slot,$referenceId = null,$reverseSlot = null)
    {
        if ($referenceId) {
            $this->__process['inputIds'][$slot] = [$referenceId=>$reverseSlot];
        } else {
            unset($this->__process['inputIds'][$slot]);
        }
    }

    /**
     * @param $slot
     * @param $referenceId
     * @param $reverseSlot
     * @return void
     */
    public function __setOutputIdForSlot($slot,$referenceId = null,$reverseSlot = null)
    {
        if ($referenceId) {
            $this->__process['outputIds'][$slot] = [$referenceId=>$reverseSlot];
        } else {
            unset($this->__process['outputIds'][$slot]);
        }
    }

    /**
     * @return array
     */
    public function __getInputIds()
    {
        return $this->__process['inputIds'];
    }

    /**
     * @return array
     */
    public function __getOutputIds()
    {
        return $this->__process['outputIds'];
    }

    /**
     * @param array $elems
     * @return void
     */
    public function __setInputIds($elems)
    {
        $this->__process['inputIds'] = $elems;
    }

    /**
     * @param array $elems
     * @return void
     */
    public function __setOutputIds($elems)
    {
        $this->__process['outputIds'] = $elems;
    }

    /**
     * @param $channel
     * @param $result
     * @return void
     */
    public function __setResult($channel,$result)
    {
        $this->__process['results'][$channel] = $result;
    }

    /**
     * @return array
     */
    public function __getRawResults()
    {
        return $this->__process['results'];
    }

    /**
     * @return int
     */
    public function __getActiveOutput()
    {
        return 1;
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        throw new NotImplementedException();
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        throw new NotImplementedException();
    }

    /**
     * @param ITreeContext $context
     * @return void
     */
    public function __setContext(ITreeContext $context)
    {
        $this->__process['context'] = $context;
    }

    /**
     * @return ITreeContext
     */
    public function __getContext()
    {
        return $this->__process['context'];
    }

    /**
     * @return mixed
     */
    public function __getConfig()
    {
        return $this->__process['config'];
    }

    /**
     * @return mixed
     */
    public function __setConfig($config)
    {
        $this->__process['config'] = $config;
    }

    /**
     * @return bool
     */
    public function __isNoParentCheck()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function __forceContinueProcess()
    {
        return false;
    }

    /**
     * @return mixed
     */
    public function __beforeStart()
    {
    }

    /**
     * @return []
     */
    public function __getDebugMessages()
    {
        return [];
    }

    /**
     * @param bool $dryMode
     */
    public function __execute($dryMode)
    {
    }
}
