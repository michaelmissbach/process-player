<?php

namespace Missbach\ProcessBundle\Core\Factory;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Collections\ObjectCollection;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Interfaces\IUnResolvable;
use Missbach\ProcessBundle\Core\Tools\StringHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConnectableTypesFactory
 * @package Missbach\ProcessBundle\Services
 */
class ConnectableTypesFactory
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var ObjectCollection
     */
    protected $collection;

    /**
     * ConnectableTypesFactory constructor.
     */
    public function __construct($container)
    {
        $this->container = $container;
        $this->collection = new ObjectCollection();
    }

    /**
     * @param $id
     * @param $definition
     * @throws \Exception
     */
    public function add($id,$definition)
    {
        unset($definition['arguments']);

        $informations = [];

        if (is_subclass_of($definition['class'],IResolvable::class)) {
            $informations = [
                'service_id'=>$id,
                'inputs' => 1,
                'outputs' => 1,
                'inputNames' => [],
                'outputNames' => [],
                'category' => 'Standard',
                'name' => '',
                'editable' => false,
                'isresolvable' => true
            ];
            if (defined(sprintf('%s::%s',$definition['class'],'__INPUTS'))) {
                $informations['inputs']=($definition['class'])::__INPUTS;
            }
            if (defined(sprintf('%s::%s',$definition['class'],'__OUTPUTS'))) {
                $informations['outputs']=($definition['class'])::__OUTPUTS;
            }

            if (defined(sprintf('%s::%s',$definition['class'],'__INPUTNAMES'))) {
                $informations['inputNames']=($definition['class'])::__INPUTNAMES;
            }
            if (defined(sprintf('%s::%s',$definition['class'],'__OUTPUTNAMES'))) {
                $informations['outputNames']=($definition['class'])::__OUTPUTNAMES;
            }
        } else if (is_subclass_of($definition['class'],IUnResolvable::class)) {
            $informations = [
                'service_id'=>$id,
                'category' => 'Standard',
                'name' => '',
                'editable' => false,
                'isresolvable' => false
            ];
            //todo: add more informations
        }

        $classSegments = explode('\\',$definition['class']);
        $className = end($classSegments);
        if (is_callable([$definition['class'],'__getName'])) {
            $name = strtolower(($definition['class'])::__getName());
        } else if (defined(sprintf('%s::%s',$definition['class'],'__NAME'))) {
            $name = strtolower(($definition['class'])::__NAME);
        } else if (StringHelper::endsWith($className, 'Connectable')) {
            $name = strtolower(str_replace('Connectable','',$className));
        }  else {
            throw new \Exception(sprintf('Classname "%s" must end with "Connectable" or implement static method "__getName".',$className));
        }
        $informations['name'] = ucfirst($name);

        if (defined(sprintf('%s::%s',$definition['class'],'__CATEGORY'))) {
            $informations['category']=ucfirst(($definition['class'])::__CATEGORY);
        }

        if (is_callable([$definition['class'],'__getEditor'])) {
            $informations['editable'] = true;
        }

        $this->collection->addItem(sprintf('%s',$informations['name']),array_merge($definition,$informations));
    }

    /**
     * @param $name
     * @return IConnectable
     */
    public function getByName($name)
    {
        $objectInformations = $this->collection->getItem($name);

        /** @var IConnectable $elem */
        $elem = $this->container->get($objectInformations['service_id']);
        $elem->__setProcessMeta('objectInformations',$objectInformations);

        return $elem;
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->collection->getItems();
    }
}
