<?php

namespace Missbach\ProcessBundle\Core\Exceptions;

/**
 * Class NoEntryFoundExeption
 * @package Missbach\ProcessBundle\Core\Exceptions
 */
class NoEntryFoundExeption extends \Exception
{
}