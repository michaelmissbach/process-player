<?php

namespace Missbach\ProcessBundle\Core\Exceptions;

/**
 * Class NotAllowedTypeException
 * @package Symfony\Component\Process\Core\Exceptions
 */
class NotImplementedException extends \Exception
{
}
