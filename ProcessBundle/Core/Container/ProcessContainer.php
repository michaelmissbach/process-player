<?php

namespace Missbach\ProcessBundle\Core\Container;

use Missbach\ProcessBundle\Core\Collections\ProcessCollection;
use Missbach\ProcessBundle\Core\Exceptions\ProcessNotFoundException;
use Missbach\ProcessBundle\Core\Interfaces\ICollection;
use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IConstructModifyable;
use Missbach\ProcessBundle\Core\Interfaces\IContainerLoader;
use Missbach\ProcessBundle\Core\Interfaces\IProcessContainerLoader;
use Missbach\ProcessBundle\Core\ProcessStructureTree\Element;
use Missbach\ProcessBundle\Modules\Virtual\NotfoundConnectable;
use Symfony\Bundle\WebProfilerBundle\Tests\Resources\IconTest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;

/**
 * Class ProcessContainer
 * @package Missbach\ProcessBundle\Core\Container
 */
class ProcessContainer
{
    /**
     * @var array
     */
    protected $processes = [];

    /**
     * @var Element
     */
    protected $treeRoot;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var IProcessContainerLoader
     */
    protected $loader;

    /**
     * ProcessContainer constructor.
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container=$container;
    }

    /**
     * @param IContainerLoader $loader
     */
    public function load()
    {
        try {
            $this->loader = $this->container->get('process.container.loader');
            $this->loader->load();
            $this->treeRoot = $this->loader->getTreeStructure();
            $this->processes = $this->loader->getFlat();
            if (!is_array($this->processes)) {
                $this->processes = [];
            }
        } catch(\Exception $e) {
            $this->processes = [];
        }

        $this->mergeLoadings();
        
        $this->preSave();
    }

    /**
     * @return void
     */
    protected function mergeLoadings()
    {
        /**
         * @var ProcessCollection $collection
         */
        foreach ($this->constructProcessContainer(unserialize($this->container->get('session')->get('processcontainer'))) as $key=>&$collection) {
            $collection->clearDirty();
            if (array_key_exists($key,$this->processes)) {
                $itemsOrig = $this->processes[$key]->getItems();
                $itemSession = $collection->getItems();
                $crcOriginal = crc32(serialize($itemsOrig));
                $crcSession = crc32(serialize($itemSession));
                $this->processes[$key] = $collection;
                if ($crcOriginal !== $crcSession) {
                    $collection->setDirty();
                }
            } else {
                $collection->setDirty();
            }

        }
    }

    /**
     * @return void
     */
    protected function preSave()
    {
        $this->container->get('session')->set('processcontainer',serialize($this->destructProcessContainer($this->processes)));
    }

    /**
     * @return bool
     */
    public function hasSessionData()
    {
        return $this->container->get('session')->has('processcontainer');
    }

    /**
     * @return string
     */
    public function getUniqueId()
    {
        return sprintf('%s',uniqid());
    }

    /**
     * @param $source
     * @return array
     */
    protected function constructProcessContainer($source)
    {
        $constructed = [];
        if (is_array($source)) {
            /** @var ProcessCollection $collection */
            foreach ($source as $containerKey=>$collection) {
                $constructed[$containerKey] = new ProcessCollection();
                /** @var IConnectable $instance */
                foreach ($collection as $key=>$instanceInformations) {
                    $constructed[$containerKey]->addItem($key,$instanceInformations);
                }
            }
        }
        return $constructed;
    }

    /**
     * @param $containerKey
     * @param $key
     * @param $id
     * @param $processMetas
     */
    protected function constructSingleItem($containerKey,$key,$id,$processMetas)
    {
        /** @var IConnectable $instance */
        $instance = null;
        if ($this->container->has($id)) {
            $instance = $this->container->get($id);
            if ($instance instanceof IConstructModifyable) {
                $return = $instance->__onConstruct($instance,$id,$processMetas);
                if ($return instanceof IConnectable) {
                    $instance = $return;
                }
            }
        }

        if (!$instance) {
            $instance = $this->container->get('process.core.module.virtual.notfound');
            $processMetas['metas']['objectInformations']['orig'] = $processMetas['metas']['objectInformations']['name'];
            $processMetas['metas']['objectInformations']['name'] = 'Notfound';
        }

        $instance->__setProcessInformations($processMetas);
        $this->processes[$containerKey]->addItem($key,$instance);
    }

    /**
     * @param $builded
     * @return mixed
     */
    protected function destructProcessContainer($builded)
    {
        $destructed = [];
        /** @var ProcessCollection $collection */
        foreach ($builded as $containerKey=>$collection) {
            $destructed[$containerKey] = [];
            /** @var IConnectable $instance */
            foreach ($collection->getItems() as $key=>$instance) {
                if ($instance instanceof IConnectable) {

                    if ($instance instanceof IConstructModifyable) {
                        $instance->__onDestruct($instance);
                    }
                    
                    $informations = $instance->__getProcessMeta('objectInformations');
                    $serviceId = $informations['service_id'];
                    $metas = $instance->__getProcessInformations();
                    
                    if ($instance instanceof NotfoundConnectable) {
                        $metas['metas']['objectInformations']['name'] = $metas['metas']['objectInformations']['orig'];
                    }
                    
                } else {
                    foreach ($instance as $id=>$processMetas) {
                        $serviceId = $id;
                        $metas = $processMetas;
                        
                        if ($metas['metas']['objectInformations']['name'] === 'Notfound') {
                            $metas['metas']['objectInformations']['name'] = $metas['metas']['objectInformations']['orig'];
                        }
                    }
                }

                $destructed[$containerKey][$key][$serviceId] = $metas;
            }
        }
        return $destructed;
    }

    /**
     * @param $containerId
     * @return mixed
     */
    public function get($containerId)
    {
        if ($this->hasCollection($containerId)) {
            $items = $this->processes[$containerId]->getItems();
            foreach ($items as $key=>$instanceInformations) {
                if ($instanceInformations instanceof IConnectable) {
                    continue;
                }
                foreach ($instanceInformations as $id=>$processMetas) {
                    $this->constructSingleItem($containerId,$key,$id,$processMetas);
                }
            }
            return $this->processes[$containerId]->getItems();
        }
        throw new ProcessNotFoundException(sprintf('Process with id "%s" not found.',$containerId));
    }

    /**
     * @param $list
     */
    public function prepareExecution(&$list)
    {
        foreach ($list as $key=>&$item) {
            if ($item instanceof IConnectable) {
                try {
                    $item->__beforeStart();
                } catch(\Exception $e) {
                    //todo: logging or what?
                }
            }
        }
    }

    /**
     * @param $containerId
     * @param $elemId
     * @return mixed
     */
    public function hasCollection($containerId)
    {
        if (array_key_exists($containerId,$this->processes)) {
            return true;
        }
        return false;
    }

    /**
     * @param $containerId
     * @param $elemId
     * @return mixed
     */
    public function isCollectionDirty($containerId)
    {
        if ($this->hasCollection($containerId)) {
            return $this->processes[$containerId]->isDirty();
        }
    }

    /**
     * @param $containerId
     * @param $elemId
     * @return mixed
     */
    public function clearCollectionDirty($containerId)
    {
        if ($this->hasCollection($containerId)) {
            $this->processes[$containerId]->clearDirty();
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getProcessContainers()
    {
        return array_keys($this->processes);
    }

    /**
     * @param $containerId
     * @return ProcessCollection
     */
    public function getProcessCollection($containerId)
    {
        if ($this->hasCollection($containerId)) {
            return $this->processes[$containerId];
        }
        return null;
    }

    /**
     * @param $containerId
     * @param $elemId
     * @return mixed
     */
    public function addElementByContainerId($containerId,$elemId,$elem)
    {
        if ($this->hasCollection($containerId)) {
            $this->processes[$containerId]->addItem($elemId,$elem);
            $this->preSave();
            return true;
        }
        return false;
    }

    /**
     * @param $containerId
     * @param $elemId
     * @return mixed
     */
    public function getElementByContainerIdAndElementId($containerId,$elemId)
    {
        if ($this->hasCollection($containerId)) {
            $instance = $this->processes[$containerId]->getItem($elemId);
            if (!($instance instanceof IConnectable)) {
                foreach ($instance as $id=>$processMetas) {
                    $this->constructSingleItem($containerId,$elemId,$id,$processMetas);
                }
                $instance = $this->processes[$containerId]->getItem($elemId);
            }
            return $instance;
        }
        return null;
    }

    /**
     * @param $containerId
     * @param $processId
     * @param IConnectable $connectable
     */
    public function add($containerId, IConnectable $connectable, $processId = null)
    {
        if (!$this->hasCollection($containerId)) {
            $this->processes[$containerId] = new ProcessCollection();
        }
        if(!$processId) {
            $processId = $this->getUniqueId();
        }

        $this->processes[$containerId]->addItem($processId,$connectable);

        $this->preSave();
        return $processId;
    }

    /**
     * @param $containerId
     * @param $elemId
     * @return bool
     */
    public function removeElementById($containerId,$elemId)
    {
        if ($this->hasCollection($containerId)) {
            $this->processes[$containerId]->removeItem($elemId);
            $this->cleanUpOrphanedConnections($containerId);
            $this->preSave();
            return true;
        }
        return false;
    }

    /**
     * @param $containerId
     * @return bool
     */
    public function cleanUpOrphanedConnections($containerId)
    {
        //todo: check for type
        if ($this->hasCollection($containerId)) {
            $list = $this->get($containerId);
            /** @var IConnectable $value */
            foreach ($this->get($containerId) as $key=>&$value) {
                $outputs = $value->__getOutputIds();
                foreach($outputs as $innerKey=>$innerValue) {
                    foreach($innerValue as $innerKey1=>$innerValue1) {
                        if (!array_key_exists($innerKey1,$list)) {
                            unset($outputs[$innerKey]);
                        }
                    }
                }
                $value->__setOutputIds($outputs);
                $inputs = $value->__getInputIds();
                foreach($inputs as $innerKey=>$innerValue) {
                    foreach($innerValue as $innerKey1=>$innerValue1) {
                        if (!array_key_exists($innerKey1,$list)) {
                            unset($inputs[$innerKey]);
                        }
                    }
                }
                $value->__setInputIds($inputs);
            }
            $this->preSave();
            return true;
        }
        return false;
    }

    /**
     * @param $containerId
     * @param $elemId1
     * @param $outputChannel
     * @param $elemId2
     * @param $inputChannel
     * @return bool
     */
    public function connectElements($containerId,$elemId1,$outputChannel,$elemId2,$inputChannel)
    {
        if (!$this->hasCollection($containerId)) {
            return false;
        }

        /** @var IConnectable $elem1 */
        $elem1 = $this->getElementByContainerIdAndElementId($containerId,$elemId1);
        /** @var IConnectable $elem2 */
        $elem2 = $this->getElementByContainerIdAndElementId($containerId,$elemId2);

        //todo: check type!
        if (!($elem1 instanceof IConnectable) || !($elem2 instanceof IConnectable)) {
            return false;
        }

        $elem1->__setOutputIdForSlot($outputChannel,$elemId2,$inputChannel);
        $elem2->__setInputIdForSlot($inputChannel,$elemId1,$outputChannel);
        $this->preSave();

        return true;
    }

    /**
     * @param $containerId
     * @param $elemId1
     * @param $outputChannel
     * @param $elemId2
     * @param $inputChannel
     * @return bool
     */
    public function unconnectElements($containerId,$elemId1,$outputChannel,$elemId2,$inputChannel)
    {
        if (!$this->hasCollection($containerId)) {
            return false;
        }

        /** @var IConnectable $elem1 */
        $elem1 = $this->getElementByContainerIdAndElementId($containerId,$elemId1);
        /** @var IConnectable $elem2 */
        $elem2 = $this->getElementByContainerIdAndElementId($containerId,$elemId2);

        //todo: check for type
        if (!($elem1 instanceof IConnectable) || !($elem2 instanceof IConnectable)) {
            return false;
        }

        $elem1->__setOutputIdForSlot($outputChannel,null);
        $elem2->__setInputIdForSlot($inputChannel,null);
        $this->preSave();

        return true;
    }

    /**
     * @param null $node
     * @param array $return
     * @return array
     */
    public function getTreeForForFrontend($node = null,&$return = [])
    {
        if (!$node) {
            $node = $this->treeRoot;
        }

        $return['id'] = $node->getId();
        $return['text'] = sprintf('%s%s',$node->getShownName(),$this->processes[$node->getId()]->isDirty() ? ' *':'');
        $return['state'] = [];
        $return['state']['opened'] = true;
        $return['state']['modified'] = $this->processes[$node->getId()]->isDirty();
        $return['state']['shownName'] = $node->getShownName();
        $return['state']['callName'] = $node->getCallName();
        $return['children'] = [];

        if ($node->hasChildren()) {
            /**
             * @var  $key
             * @var Element $element
             */
            foreach ($node->getChildren() as $key=>$element) {
                $this->getTreeForForFrontend($element,$return['children'][]);
            }
        }

        return $return;
    }

    /**
     * @param null $node
     * @param $id
     * @param Element $return
     */
    public function findByIdInTree($node = null,$id,&$return = null)
    {
        if (!$node) {
            $node = $this->treeRoot;
        }

        if ($return) {
            return;
        }

        if ($node->getId() === $id) {
            $return = $node;
            return;
        }

        if ($node->hasChildren()) {
            /**
             * @var  $key
             * @var Element $element
             */
            foreach ($node->getChildren() as $key=>$element) {
                $this->findByIdInTree($element,$id,$return);
            }
        }
    }

    /**
     * @param null $node
     * @param $id
     * @param Element $return
     */
    public function findByIdWithChildrenInTreeList($node = null,&$return = null)
    {
        if (!$node) {
            $node = $this->treeRoot;
        }

        $return[$node->getId()] = $node;

        if ($node->hasChildren()) {
            /**
             * @var  $key
             * @var Element $element
             */
            foreach ($node->getChildren() as $key=>$element) {
                $this->findByIdWithChildrenInTreeList($element,$return);
            }
        }
    }

    /**
     * @param $parentId
     * @return void
     */
    public function addNewContainerByParentId($parentId)
    {
        $this->findByIdInTree(null,$parentId,$parent);

        /** @var Element $parent */
        if ($parent) {
            $element = new Element();
            $element->setParent($parent);
            $parent->addChild($element);

            $this->container->get('process.container.loader')->save($this->treeRoot);
        }
    }

    /**
     * @param $containerId
     * @param null $shownName
     * @param null $callName
     */
    public function renameContainer($containerId,$shownName = null, $callName = null)
    {
        $this->findByIdInTree(null,$containerId,$element);
        $listElement = $this->processes[$containerId];

        if (strlen($shownName)) {
            $element->setShownName($shownName);
        }
        if (strlen($callName)) {
            $element->setCallName($callName);
        }

        $this->saveContainerAndOverwriteUnsafedState($containerId);
    }

    /**
     * @param $containerId
     */
    public function removeRecursiveInTree($containerId)
    {
        $this->findByIdInTree(null,$containerId,$element);
        $this->findByIdWithChildrenInTreeList($element,$list);

        foreach ($list as $key=>$elem) {
            unset($this->processes[$key]);
        }
        $element->getParent()->removeChild($element);
        $this->preSave();
        $this->container->get('process.container.loader')->save($this->treeRoot);
    }

    /**
     * @param $containerId
     */
    public function saveContainerAndOverwriteUnsafedState($containerId)
    {
        $this->findByIdInTree(null,$containerId, $element);
        $destructed = $this->destructProcessContainer($this->processes);
        $elementToSave = $destructed[$containerId];

        $element->setData($elementToSave);
        $this->container->get('process.container.loader')->save($this->treeRoot);
    }

    /**
     * @param $containerId
     */
    public function loadContainerAndOverwriteUnsafedState($containerId)
    {
        $origElements = $this->container->get('process.container.loader')->getFlat();
        $neededElement = $origElements[$containerId];

        $this->findByIdInTree(null,$containerId,$element);
        $element->setData($neededElement->getItems());

        /** @var ProcessCollection $collection */
        $collection = $this->processes[$containerId];
        $collection->removeItems();
        foreach($neededElement->getItems() as $key=>$value) {
            $collection->addItem($key,$value);
        }
        $collection->clearDirty();
        $this->processes[$containerId] = $collection;
        $this->preSave();
        $this->container->get('process.container.loader')->save($this->treeRoot);
    }
}
