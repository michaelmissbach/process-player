<?php

namespace Missbach\ProcessBundle\Core\Collections;

use Missbach\ProcessBundle\Core\Abstracts\AbstractCollection;

/**
 * Class ProcessCollection
 * @package Missbach\ProcessBundle\Core\Collections
 */
class ProcessCollection extends AbstractCollection
{
}