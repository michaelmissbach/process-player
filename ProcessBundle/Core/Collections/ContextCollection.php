<?php

namespace Missbach\ProcessBundle\Core\Collections;

use Missbach\ProcessBundle\Core\Abstracts\AbstractCollection;

/**
 * Class ContextCollection
 * @package Missbach\ProcessBundle\Core\Collections
 */
class ContextCollection extends AbstractCollection
{
    /**
     * @return array
     */
    public function getKeys()
    {
        return array_keys($this->collection);
    }
}
