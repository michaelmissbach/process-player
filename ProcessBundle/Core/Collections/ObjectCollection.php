<?php

namespace Missbach\ProcessBundle\Core\Collections;

use Missbach\ProcessBundle\Core\Abstracts\AbstractCollection;

/**
 * Class ObjectCollection
 * @package Symfony\Component\Process\Core\Collections
 */
class ObjectCollection extends AbstractCollection
{
}