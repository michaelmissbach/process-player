<?php

namespace Missbach\ProcessBundle\Services;

use Missbach\ProcessBundle\Core\Container\ProcessContainer;
use Missbach\ProcessBundle\Core\Exceptions\NoEntryFoundExeption;
use Missbach\ProcessBundle\Core\Exceptions\ProcessNotFoundException;
use Missbach\ProcessBundle\Core\Factory\ConnectableTypesFactory;
use Missbach\ProcessBundle\Core\ExecutedTree\Builder;
use Missbach\ProcessBundle\Core\Interfaces\IProcessContainerLoader;
use Missbach\ProcessBundle\Core\Interfaces\ITreeContext;

/**
 * Class ProcessService
 * @package Missbach\ProcessBundle\Services
 */
class ProcessService
{
    const BUNDLE_NAME = 'Visual Process';

    const AUTHOR = 'Mißbach Michael';

    const BUILD_DATE = '2018-06-11';

    const VERSION_MAJOR = 1;

    const VERSION_MINOR = 0;

    const VERSION_REV = 12;

    const VERSION_ADDITION = 'beta';

    /**
     * @return string
     */
    public static function getVersionString()
    {
        return sprintf(
            '%s.%s.%s%s',
            self::VERSION_MAJOR,
            self::VERSION_MINOR,
            self::VERSION_REV,
            strlen(self::VERSION_ADDITION)? sprintf(' %s',self::VERSION_ADDITION) : ''
        );
    }

    /**
     * @return string
     */
    public static function getCopyrightString()
    {
        return sprintf('Copyright %s (Build Date %s)',self::AUTHOR,self::BUILD_DATE);
    }

    /**
     * @return string
     */
    public static function getBundleNameString()
    {
        return sprintf('%s Core (Version %s)',self::BUNDLE_NAME,self::getVersionString());
    }

    /**
     * @var string
     */
    protected static $currentContainerId = null;

    /**
     * @return string
     */
    public static function getCurrentContainerId()
    {
        return self::$currentContainerId;
    }

    /**
     * @param string $currentContainerId
     */
    public static function setCurrentContainerId(string $currentContainerId)
    {
        self::$currentContainerId = $currentContainerId;
    }

    /**
     * @var ConnectableTypesFactory
     */
    protected $factory;

    /**
     * @var ProcessContainer
     */
    protected $processContainer;

    /**
     * @var Builder
     */
    protected $treeBuilder;

    /**
     * @var IProcessContainerLoader
     */
    protected $loader;

    /**
     * @return ConnectableTypesFactory
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @return ProcessContainer
     */
    public function getProcessContainer()
    {
        return $this->processContainer;
    }

    /**
     * @return Builder
     */
    public function getTreeBuilder()
    {
        return $this->treeBuilder;
    }
    
    /**
     * @return IProcessContainerLoader
     */
    public function getLoader()
    {
        return $this->loader;
    }

    /**
     * ProcessService constructor.
     * @param ConnectableTypesFactory $factory
     * @param ProcessContainer $processContainer
     */
    public function __construct(
        ConnectableTypesFactory $factory, 
        ProcessContainer $processContainer, 
        Builder $treeBuilder,
        IProcessContainerLoader $loader)
    {
        $this->factory          = $factory;
        $this->processContainer = $processContainer;
        $this->treeBuilder      = $treeBuilder;
        $this->loader           = $loader;
    }

    /**
     * @param $name
     * @param bool $dryMode
     * @return array
     * @throws ProcessNotFoundException
     */
    public function executeProcessByName($name, $dryMode = false, ITreeContext $context = null)
    {
        $this->loader->load();

        foreach($this->loader->getListWithMetas() as $id=>$metas) {
            if (trim($metas['callName']) === trim($name)) {
                return $this->executeProcessById($id, $dryMode, $context);
            }
        }

        throw new ProcessNotFoundException(sprintf('Process with name "%s" not found.',$name));
    }

    /**
     * @param $id
     * @param bool $dryMode
     * @return array
     */
    public function executeProcessById($id, $dryMode = false, ITreeContext $context = null)
    {
        $errorMessage = '';

        try {
            $this->loader->load();
            $this->processContainer->load();
            $this->runProcess($id,$dryMode,$context);

        } catch (\Exception $e) {
            $errorMessage = $e->getMessage();
        }

        return [
            'result' => $this->treeBuilder->getResult(),
            'trace' => $this->treeBuilder->getTrace(),
            'ids' => $this->treeBuilder->getFinishedId(),
            'error' => $errorMessage
        ];
    }

    /**
     * @param $id
     * @param bool $dryMode
     * @param ITreeContext|null $context
     * @throws NoEntryFoundExeption
     * @throws ProcessNotFoundException
     * @throws \Missbach\ProcessBundle\Core\Exceptions\CircularProcessObjectException
     */
    public function runProcess($id, $dryMode = false, ITreeContext $context = null)
    {
        $list = $this->processContainer->get($id);

        if (!count($list)) {
            throw new NoEntryFoundExeption('There are no objects.');
        }

        $this->treeBuilder->createTree($list, $context);
        $this->processContainer->prepareExecution($list);

        $start = $this->treeBuilder->getStart();
        if (!$start) {
            throw new NoEntryFoundExeption('There is no defined startpoint.');
        }

        $processMetas = $this->loader->getProcessById($id);

        Builder::setDryMode($dryMode);
        Builder::addTrace(sprintf('Start process "%s".',$processMetas['shownName']));

        $start->resolve();
        return $this->treeBuilder->getResult();
    }
}
