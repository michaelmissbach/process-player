<?php

namespace Missbach\ProcessBundle\Command;

use Missbach\ProcessBundle\Services\ProcessService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AbstractDebugCommand
 * @package Missbach\ProcessBundle\Command
 */
class AbstractDebugCommand extends ContainerAwareCommand
{
    /**
     * @var integer
     */
    const MAX_CHARS_IN_LINE = 120;
    
    /**
     * @param $input
     * @param $charCount
     * @param string $fillChar
     * @return bool|string
     */
    protected function formatToExactCharCount($input,$charCount,$fillChar = ' ')
    {
        $length = strlen($input);
        if ($length > $charCount) {
            return '...'.substr($input,strlen($input) - ($charCount - 3),$charCount);
        }

        while($length < $charCount) {
            $input .= $fillChar;
            $length = strlen($input);
        }

        return $input;
    }

    /**
     * @param OutputInterface $output
     */
    protected function printOutputHeader(OutputInterface $output)
    {
        $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE,'*'));
        $output->writeln(sprintf('%s (Version %s)',ProcessService::BUNDLE_NAME,ProcessService::getVersionString()));
        $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE,'*'));
        $output->writeln('');
    }
    
    /**
     * @param OutputInterface $output
     */
    protected function printOutputFooter(OutputInterface $output)
    {
        $output->writeln('');
        $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE,'*'));
        $output->writeln(sprintf('Copyright %s (Build Date %s)',ProcessService::AUTHOR,ProcessService::BUILD_DATE));
        $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE,'*'));
        $output->writeln('');
    }
}
