<?php

namespace Missbach\ProcessBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DebugCommand
 * @package Missbach\ProcessBundle\Command
 */
class DebugListProcessesCommand extends AbstractDebugCommand
{
    /**
     *
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('vprocess:list:processes');

        $this->setDescription('Prints debug informations.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->printOutputHeader($output);

        $output->writeln('Currently registered processes:');
        $output->writeln('');

        $output->writeln(
            $this->formatToExactCharCount('Id',20).
            $this->formatToExactCharCount('Title',40).
            $this->formatToExactCharCount('Callname',60)
        );
        $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE, '='));

        $this->getContainer()->get('process.container.loader')->load();

        foreach($this->getContainer()->get('process.container.loader')->getListWithMetas() as $name=>$single) {
            $output->writeln(
                $this->formatToExactCharCount($single['id'],19).' ' .
                $this->formatToExactCharCount($single['shownName'],39). ' ' .
                $this->formatToExactCharCount($single['callName'],59)
            );
        }

        $this->printOutputFooter($output);
    }
}
