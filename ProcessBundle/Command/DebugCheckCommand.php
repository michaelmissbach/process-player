<?php

namespace Missbach\ProcessBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class DebugCommand
 * @package Missbach\ProcessBundle\Command
 */
class DebugCheckCommand extends AbstractDebugCommand
{
    /**
     *
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('vprocess:check:process');

        $this->setDescription('Prints debug informations.');

        $this->addArgument('name', InputArgument::REQUIRED, 'Specify process to start.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->printOutputHeader($output);

        $service = $this->getContainer()->get('process.service');

        try {


            $result = $service->executeProcessByName($input->getArgument('name'),true);

            $output->writeln(sprintf('The output result is "%s".',$result['result'] ? 'true':'false'));
            if (strlen($result['error'])) {
                $output->writeln(sprintf('An error occured: %s.',$result['error']));
            }
            $output->writeln('');
            $output->writeln('Trace:');
            foreach ($result['trace'] as $trace) {
                $output->writeln($trace);
            }
            $output->writeln('');
            $output->writeln('Visited Ids:');
            foreach (array_keys($result['ids']) as $id) {
                $output->writeln($id);
            }

        } catch(\Exception $e) {
            $output->writeln($e->getMessage());
        }

        $this->printOutputFooter($output);
    }
}
