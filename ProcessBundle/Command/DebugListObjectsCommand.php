<?php

namespace Missbach\ProcessBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DebugCommand
 * @package Missbach\ProcessBundle\Command
 */
class DebugListObjectsCommand extends AbstractDebugCommand
{
    /**
     *
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('vprocess:list:objects');

        $this->setDescription('Prints debug informations.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->printOutputHeader($output);

        $output->writeln('Currently registered connectable classes:');
        $output->writeln('');

        $output->writeln(
            $this->formatToExactCharCount('Name',20).
            $this->formatToExactCharCount('Id',40).
            $this->formatToExactCharCount('Class',60)
        );
        $output->writeln($this->formatToExactCharCount('',self::MAX_CHARS_IN_LINE, '='));

        foreach($this->getContainer()->get('process.core.factory')->getAll() as $name=>$single) {
            $output->writeln(
                $this->formatToExactCharCount($name,19).' ' .
                $this->formatToExactCharCount($single['service_id'],39). ' ' .
                $this->formatToExactCharCount($single['class'],59)
            );
        }

        $this->printOutputFooter($output);
    }
}
