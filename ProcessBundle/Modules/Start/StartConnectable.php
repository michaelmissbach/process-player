<?php

namespace Missbach\ProcessBundle\Modules\Start;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AndConnectable
 * @package Missbach\ProcessBundle\Objects
 */
class StartConnectable implements IConnectable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 0;
    const __CATEGORY = 'Start';

    /**
     * @var ProcessEditorService
     */
    protected $editorService;

    /**
     * StartConnectable constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        if ($container->has('process.editor.service')) {
            $this->editorService = $container->get('process.editor.service');
        }
    }

    /**
     * @return mixed|void
     */
    public function __beforeStart()
    {
        $context = $this->__getContext();
        $config = $this->__getConfig();
        foreach ($config as $key=>$value) {
            $context->getParameterCollection()->addItem(trim($key),null);
        }
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        return true;
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        return true;
    }

    /**
     * @return string
     */
    public function __getEditor()
    {
        return $this->editorService->render(
            '@VisualProcessModule/Start/Resources/views/editor.html.twig',
            [
                'config'=>$this->__getConfig()
            ]
        );
    }
}
