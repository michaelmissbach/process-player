<?php

namespace Missbach\ProcessBundle\Modules\Setter\ValueTypes;

use Missbach\ProcessBundle\Modules\Setter\Interfaces\IValueType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BoolValueType
 * @package Missbach\ProcessBundle\Modules\Setter\ValueTypes
 */
class BoolValueType implements IValueType
{
    /**
     * @var null
     */
    protected $value;

    /**
     * @return string
     */
    public function getName()
    {
        return 'Bool';
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param null $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return bool
     */
    public function transformValue()
    {
        switch(true) {
            case strtolower($this->value) === 'true':
                return true;
            case strtolower($this->value) === 'false':
                return false;
            default:
                return (bool)$this->value;
        }
    }

    /**
     * @param ContainerInterface $container
     * @return array|mixed
     */
    public function getExistingValues(ContainerInterface $container)
    {
        return ['true','false'];
    }
}