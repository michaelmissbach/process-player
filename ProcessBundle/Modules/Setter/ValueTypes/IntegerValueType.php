<?php

namespace Missbach\ProcessBundle\Modules\Setter\ValueTypes;

use Missbach\ProcessBundle\Modules\Setter\Interfaces\IValueType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BoolValueType
 * @package Missbach\ProcessBundle\Modules\Setter\ValueTypes
 */
class IntegerValueType implements IValueType
{
    /**
     * @var null
     */
    protected $value;

    /**
     * @return string
     */
    public function getName()
    {
        return 'Integer';
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param null $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function transformValue()
    {
        return (int)$this->value;
    }

    /**
     * @param ContainerInterface $container
     * @return array|mixed
     */
    public function getExistingValues(ContainerInterface $container)
    {
        return [];
    }
}