<?php

namespace Missbach\ProcessBundle\Modules\Setter\ValueTypes;

use Missbach\ProcessBundle\Modules\Setter\Interfaces\IValueType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BoolValueType
 * @package Missbach\ProcessBundle\Modules\Setter\ValueTypes
 */
class StringValueType implements IValueType
{
    /**
     * @var null
     */
    protected $value;

    /**
     * @return string
     */
    public function getName()
    {
        return 'String';
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param null $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function transformValue()
    {
        return (string)$this->value;
    }

    /**
     * @param ContainerInterface $container
     * @return array|mixed
     */
    public function getExistingValues(ContainerInterface $container)
    {
        return [];
    }
}