<?php

namespace Missbach\ProcessBundle\Modules\Setter\Services;

use Missbach\ProcessBundle\Modules\Setter\Interfaces\IValue;
use Missbach\ProcessBundle\Modules\Setter\Interfaces\IValueType;

/**
 * Class SetterService
 * @package Missbach\ProcessBundle\Modules\Setter\Services
 */
class SetterService
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $valueProviders = [];

    /**
     * @var array
     */
    protected $valueTypes = [];

    /**
     * SetterService constructor.
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @param $valueTypeName
     * @param $id
     */
    public function addValueType($className,$id)
    {
        $this->valueTypes[$className::getName()] = $id;
    }

    /**
     * @return array
     */
    public function getValueTypeNames()
    {
        return array_keys($this->valueTypes);
    }

    /**
     * @return array
     */
    public function getValueTypes()
    {
        return array_keys($this->valueTypes);
    }

    /**
     * @param $name
     * @param $value
     * @return IValue
     * @throws \Exception
     */
    public function getValueTypeByName($name,$value = null)
    {
        if (array_key_exists($name,$this->valueTypes)) {
            /** @var IValue $instance */
            $instance = $this->container->get($this->valueTypes[$name]);
            $instance->setValue($value);
            return $instance;
        }
        throw new \Exception(sprintf('Valuetype with name "%s" not found.',$name));
    }

    /**
     * @param $className
     * @param $id
     */
    public function addValueProvider($className, $id)
    {
        $this->valueProviders[$className::getName()] = $id;
    }

    /**
     * @return array
     */
    public function getValueProviderNames()
    {
        return array_keys($this->valueProviders);
    }

    /**
     * @param $name
     * @param null $value
     * @return IValue
     * @throws \Exception
     */
    public function getValueProviderByName($name,$value = null)
    {
        if (array_key_exists($name,$this->valueProviders)) {
            /** @var IValue $instance */
            $instance = $this->container->get($this->valueProviders[$name]);
            $instance->setValue($value);
            return $instance;
        }
        throw new \Exception(sprintf('Valueprovider with name "%s" not found.',$name));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getAllValueProviderTerms()
    {
        $result = [];
        foreach ($this->valueProviders as $key=>$provider) {
            /** @var IValue $valueProvider */
            $valueProvider = $this->getValueProviderByName($key);
            $result[$key] = $valueProvider->getExistingValues($this->container);
        }
        return $result;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getAllValueTypeTerms()
    {
        $result = [];
        foreach ($this->valueTypes as $key=>$type) {
            /** @var IValueType $valueProvider */
            $valueType = $this->getValueTypeByName($key);
            $result[$key] = $valueType->getExistingValues($this->container);
        }
        return $result;
    }

    /**
     * @param IValue $value
     * @param IValueType $valueType
     */
    public function execute(IValue $value, IValueType $valueType)
    {
        $value->executeValue($valueType->transformValue());
    }
}
