<?php

namespace Missbach\ProcessBundle\Modules\Setter;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;
use Missbach\ProcessBundle\Modules\Setter\Services\SetterService;
use Missbach\ProcessEditorBundle\Services\ProcessEditorService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IfConnectable
 * @package Missbach\ProcessBundle\Modules\Comparer
 */
class SetterConnectable implements IConnectable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 1;
    const __CATEGORY = 'Variables';

    /**
     * @var ProcessEditorService
     */
    protected $editorService;

    /**
     * @var SetterService
     */
    protected $setter;

    /**
     * @var array
     */
    protected $messages = [];

    /**
     * @return array
     */
    public function __getDebugMessages()
    {
        return $this->messages;
    }

    /**
     * SetterConnectable constructor.
     * @param SetterService $setter
     * @param ContainerInterface $container
     */
    public function __construct(SetterService $setter, ContainerInterface $container)
    {
        $this->setter = $setter;

        if ($container->has('process.editor.service')) {
            $this->editorService = $container->get('process.editor.service');
        }
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        $results = $this->__getRawResults();
        return isset($results[1]);
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        $results = $this->__getRawResults();
        if (!isset($results[1])) {
            return true;
        }
        return $results[1];
    }

    /**
     * @param bool $dryMode
     * @throws \Exception
     */
    public function __execute($dryMode)
    {
        $this->resolveSetting();
    }

    /**
     * @return bool|void
     */
    public function __forceContinueProcess()
    {
        return true;
    }

    /**
     * @throws \Exception
     */
    public function resolveSetting()
    {
        try {
            $config = $this->__getConfig();
            $this->setter->execute(
                $this->setter->getValueProviderByName($config['valueprovider'],$config['valueprovider_value']),
                $this->setter->getValueTypeByName($config['valuetype'],$config['valuetype_value'])
            );
            $this->messages[] = sprintf('%s %s set to %s %s',
                $config['valueprovider'],
                $config['valueprovider_value'],
                $config['valuetype'],
                $config['valuetype_value']);
        } catch(\Exception $e) {
            throw new \Exception('SetterConnectable not valid configured.');
        }
    }

    /**
     * @return string
     */
    public function __getEditor()
    {
        return $this->editorService->render(
            '@VisualProcessModule/Setter/Resources/views/editor.html.twig',
            [
                'config'=>$this->__getConfig(),
                'setter' => $this->setter
            ]
        );
    }
}
