<?php

namespace Missbach\ProcessBundle\Modules\Setter\Values;

use Missbach\ProcessBundle\Core\ExecutedTree\Builder;
use Missbach\ProcessBundle\Modules\Setter\Interfaces\IValue;
use Missbach\ProcessBundle\Services\ProcessService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IntegerValue
 * @package Missbach\ProcessBundle\Modules\Comparer\Values
 */
class ContextValue implements IValue
{
    /**
     * @var integer
     */
    protected $value;

    /**
     * @return string
     */
    public static function getName()
    {
        return 'Contextvariable';
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param $value
     */
    public function executeValue($value)
    {
        $collection = Builder::getContext()->getParameterCollection();
        $collection->addItem($this->value,$value);
        Builder::getContext()->setParameterCollection($collection);
    }

    /**
     * @param ContainerInterface $container
     * @return array|mixed
     */
    public function getExistingValues(ContainerInterface $container)
    {
        $containerId = ProcessService::getCurrentContainerId();
        $processService = $container->get('process.service');

        $processContainer = $processService->getProcessContainer();
        $treeBuilder = $processService->getTreeBuilder();

        $processContainer->load();
        $list = $processContainer->get(ProcessService::getCurrentContainerId());
        $treeBuilder->createTree($list);
        $processContainer->prepareExecution($list);

        $context = $treeBuilder->getContextFromInstance();

        return $context->getParameterCollection()->getKeys();
    }
}
