<?php

namespace Missbach\ProcessBundle\Modules\Setter\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Class RegisterPass
 * @package Missbach\ProcessBundle\DependencyInjection
 */
class RegisterPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $this->registerValueProviders($container);
        $this->registerValueTypes($container);
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function registerValueProviders(ContainerBuilder $container)
    {
        $definition = $container->findDefinition('process.core.module.setter.service');
        $taggedServices = $container->findTaggedServiceIds('process.module.setter.valueprovider');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $tag) {
                $def = $container->findDefinition($id);
                $definition->addMethodCall('addValueProvider', array($def->getClass(),$id));
            }
        }
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function registerValueTypes(ContainerBuilder $container)
    {
        $definition = $container->findDefinition('process.core.module.setter.service');
        $taggedServices = $container->findTaggedServiceIds('process.module.setter.valuetype');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $tag) {
                $def = $container->findDefinition($id);
                $definition->addMethodCall('addValueType', array($def->getClass(),$id));
            }
        }
    }
}