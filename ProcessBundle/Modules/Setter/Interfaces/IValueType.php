<?php

namespace Missbach\ProcessBundle\Modules\Setter\Interfaces;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface IValueType
 * @package Missbach\ProcessBundle\Modules\Setter\Interfaces
 */
interface IValueType
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param $value
     */
    public function setValue($value);

    /**
     * @param $value
     */
    public function transformValue();

    /**
     * @param ContainerInterface $container
     * @return array|mixed
     */
    public function getExistingValues(ContainerInterface $container);
}