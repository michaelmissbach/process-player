<?php

namespace Missbach\ProcessBundle\Modules\Chain\Services;

use Missbach\ProcessBundle\Services\ProcessService;

/**
 * Class ChainService
 * @package Missbach\ProcessBundle\Modules\Chain\Services
 */
class ChainService
{
    /**
     * @var ProcessService
     */
    protected $processService;

    /**
     * ChainService constructor.
     * @param ProcessService $processService
     */
    public function __construct(ProcessService $processService)
    {
        $this->processService = $processService;
    }

    /**
     * @return array
     */
    public function getProcessesList()
    {
        $result = [];
        foreach ($this->processService->getLoader()->getListWithMetas() as $id=>$metas) {
            $result[$id] = $metas['shownName'];
        }
        return $result;
    }

    /**
     * @param $id
     * @param $dryMode
     * @param $context
     */
    public function runAnotherProcess($id, $dryMode,$context)
    {
        return $this->processService->runProcess($id,$dryMode,$context);
    }
}
