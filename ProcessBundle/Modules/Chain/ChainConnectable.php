<?php

namespace Missbach\ProcessBundle\Modules\Chain;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;
use Missbach\ProcessBundle\Modules\Chain\Services\ChainService;
use Missbach\ProcessEditorBundle\Services\ProcessEditorService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ChainConnectable implements IConnectable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 1;
    const __OUTPUTS = 0;
    const __CATEGORY = 'Process';

    /**
     * @var ProcessEditorService
     */
    protected $editorService;

    /**
     * @var ChainService
     */
    protected $chainService;

    /**
     * StartConnectable constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ChainService $chainService, ContainerInterface $container)
    {
        $this->chainService = $chainService;

        if ($container->has('process.editor.service')) {
            $this->editorService = $container->get('process.editor.service');
        }
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        $results = $this->__getRawResults();
        return isset($results[0]);
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        $results = $this->__getRawResults();
        return isset($results[0]) && $results[0];
    }

    /**
     * @param $dryMode
     */
    public function __execute($dryMode)
    {
        $config = $this->__getConfig();
        $this->__setResult(0,$this->chainService->runAnotherProcess($config['process'],$dryMode,$this->__getContext()));
    }

    /**
     * @return string
     */
    public function __getEditor()
    {
        return $this->editorService->render(
            '@VisualProcessModule/Chain/Resources/views/editor.html.twig',
            [
                'config'=>$this->__getConfig(),
                'processes' => $this->chainService->getProcessesList()
            ]
        );
    }
}
