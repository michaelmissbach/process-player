<?php

namespace Missbach\ProcessBundle\Modules\Comparer;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;
use Missbach\ProcessBundle\Modules\Comparer\Services\Compare;
use Missbach\ProcessEditorBundle\Services\ProcessEditorService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IfConnectable
 * @package Missbach\ProcessBundle\Modules\Comparer
 */
class IfConnectable implements IConnectable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 1;
    const __OUTPUTS = 2;
    const __CATEGORY = 'Variables';

    /**
     * @var array
     */
    protected $messages = [];

    /**
     * @var ProcessEditorService
     */
    protected $editorService;

    /**
     * @var Compare
     */
    protected $compare;

    /**
     * @var null
     */
    protected $outputChannel = null;

    /**
     * IfConnectable constructor.
     * @param Compare $compare
     * @param ContainerInterface $container
     */
    public function __construct(Compare $compare, ContainerInterface $container)
    {
        $this->compare = $compare;

        if ($container->has('process.editor.service')) {
            $this->editorService = $container->get('process.editor.service');
        }
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        $results = $this->__getRawResults();
        return isset($results[1]);
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        $results = $this->__getRawResults();
        return isset($results[1]);
    }

    /**
     * @return int
     */
    public function __getActiveOutput()
    {
        if ($this->outputChannel) {
            return $this->outputChannel;
        }

        $this->outputChannel = $this->resolveCamparison() ? 1 : 2;
        return $this->outputChannel;
    }

    /**
     * @return array
     */
    public function __getDebugMessages()
    {
        return $this->messages;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    protected function resolveCamparison()
    {
        $result = false;

        try {
            $config = $this->__getConfig();

            $result = $this->compare->getResult(
                $this->compare->getFunctionByName($config['left_function']),
                $this->compare->getValueProviderByName($config['left_valueprovider'],$config['left_value']),
                $this->compare->getOperatorBySign($config['operator']),
                $this->compare->getFunctionByName($config['right_function']),
                $this->compare->getValueProviderByName($config['right_valueprovider'],$config['right_value'])
            );
            $this->messages[] = sprintf('%s(%s %s) %s %s (%s %s) => %s',
                $config['left_function'],
                $config['left_valueprovider'],
                $config['left_value'],
                $config['operator'],
                $config['right_function'],
                $config['right_valueprovider'],
                $config['right_value'],
                $result ? 'true':'false');
        } catch(\Exception $e) {
            throw new \Exception('IfConnectable not valid configured.');
        }

        return $result;
    }

    /**
     * @return string
     */
    public function __getEditor()
    {
        return $this->editorService->render(
            '@VisualProcessModule/Comparer/Resources/views/editor.html.twig',
            [
                'config'=>$this->__getConfig(),
                'comparer' => $this->compare
            ]
        );
    }
}
