<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Operators;

use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IOperator;
use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IValue;

/**
 * Class GreaterThan
 * @package Missbach\ProcessBundle\Modules\Comparer\Operators
 */
class Equals implements IOperator
{
    /**
     * @return string
     */
    public static function getSign()
    {
        return '=';
    }

    /**
     * @param $left
     * @param $right
     * @return bool|mixed
     */
    public static function resolve($left, $right)
    {
        return $left == $right;
    }
}
