<?php

namespace Missbach\ProcessBundle\Modules\Comparer\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Class RegisterPass
 * @package Missbach\ProcessBundle\DependencyInjection
 */
class RegisterPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $this->registerOperators($container);
        $this->registerFunctions($container);
        $this->registerValueProviders($container);
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function registerOperators(ContainerBuilder $container)
    {
        $definition = $container->findDefinition('process.core.module.comparer.service');
        $taggedServices = $container->findTaggedServiceIds('process.module.comparer.operator');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $tag) {
                $def = $container->findDefinition($id);
                $definition->addMethodCall('addOperator', array($def->getClass(),$id));
            }
        }
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function registerFunctions(ContainerBuilder $container)
    {
        $definition = $container->findDefinition('process.core.module.comparer.service');
        $taggedServices = $container->findTaggedServiceIds('process.module.comparer.function');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $tag) {
                $def = $container->findDefinition($id);
                $definition->addMethodCall('addFunction', array($def->getClass(),$id));
            }
        }
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function registerValueProviders(ContainerBuilder $container)
    {
        $definition = $container->findDefinition('process.core.module.comparer.service');
        $taggedServices = $container->findTaggedServiceIds('process.module.comparer.valueprovider');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $tag) {
                $def = $container->findDefinition($id);
                $definition->addMethodCall('addValueProvider', array($def->getClass(),$id));
            }
        }
    }
}