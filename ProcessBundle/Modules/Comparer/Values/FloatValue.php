<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Values;

use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IValue;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IntegerValue
 * @package Missbach\ProcessBundle\Modules\Comparer\Values
 */
class FloatValue implements IValue
{
    /**
     * @var integer
     */
    protected $value;

    /**
     * @return string
     */
    public static function getName()
    {
        return 'Float';
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return float
     */
    public function transformValue()
    {
        return (float)$this->value;
    }

    /**
     * @param ContainerInterface $container
     * @return array|mixed
     */
    public function getExistingValues(ContainerInterface $container)
    {
        return [];
    }
}
