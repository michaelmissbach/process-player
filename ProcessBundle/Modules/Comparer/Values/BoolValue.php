<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Values;

use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IValue;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IntegerValue
 * @package Missbach\ProcessBundle\Modules\Comparer\Values
 */
class BoolValue implements IValue
{
    /**
     * @var integer
     */
    protected $value;

    /**
     * @return string
     */
    public static function getName()
    {
        return 'Bool';
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     *
     */
    public function transformValue()
    {
        switch(true) {
            case strtolower($this->value) === 'true':
                return true;
            case strtolower($this->value) === 'false':
                return false;
            default:
                return (bool)$this->value;
        }
    }

    /**
     * @param ContainerInterface $container
     * @return array|mixed
     */
    public function getExistingValues(ContainerInterface $container)
    {
        return ['true','false'];
    }
}
