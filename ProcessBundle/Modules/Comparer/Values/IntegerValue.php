<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Values;

use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IValue;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IntegerValue
 * @package Missbach\ProcessBundle\Modules\Comparer\Values
 */
class IntegerValue implements IValue
{
    /**
     * @var integer
     */
    protected $value;

    /**
     * @return string
     */
    public static function getName()
    {
        return 'Integer';
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function transformValue()
    {
        return (int)$this->value;
    }

    /**
     * @param ContainerInterface $container
     * @return array|mixed
     */
    public function getExistingValues(ContainerInterface $container)
    {
        return [];
    }
}
