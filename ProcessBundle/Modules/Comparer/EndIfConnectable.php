<?php

namespace Missbach\ProcessBundle\Modules\Comparer;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;
use Missbach\ProcessBundle\Modules\Comparer\Services\Compare;
use Missbach\ProcessEditorBundle\Services\ProcessEditorService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class IfConnectable
 * @package Missbach\ProcessBundle\Modules\Comparer
 */
class EndIfConnectable implements IConnectable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 2;
    const __OUTPUTS = 1;
    const __CATEGORY = 'Variables';

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        return true;
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        $results = $this->__getRawResults();
        if(isset($results[0])) {
            return $results[0];
        }
        return true;
    }

    /**
     * @return bool
     */
    public function __isNoParentCheck()
    {
        return true;
    }
}
