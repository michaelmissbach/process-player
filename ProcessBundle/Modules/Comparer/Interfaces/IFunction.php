<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Interfaces;

/**
 * Interface IOperator
 * @package Missbach\ProcessBundle\Modules\Comparer\Interfaces
 */
interface IFunction
{
    /**
     * @return string
     */
    public static function getName();

    /**
     * @param IValue $value
     * @return IValue
     */
    public static function execute(IValue $value);
}