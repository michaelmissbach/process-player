<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Interfaces;

/**
 * Interface IOperator
 * @package Missbach\ProcessBundle\Modules\Comparer\Interfaces
 */
interface IOperator
{
    /**
     * @return string
     */
    public static function getSign();

    /**
     * @param $left
     * @param $right
     * @return mixed
     */
    public static function resolve($left, $right);
}