<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Interfaces;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface IValue
 * @package Missbach\ProcessBundle\Modules\Comparer\Interfaces
 */
interface IValue
{
    /**
     * @return string
     */
    public static function getName();

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param $value
     */
    public function setValue($value);

    /**
     * @param $value
     */
    public function transformValue();

    /**
     * @param ContainerInterface $container
     * @return mixed
     */
    public function getExistingValues(ContainerInterface $container);
}
