<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Services;

use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IFunction;
use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IOperator;
use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IValue;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Compare
 * @package Missbach\ProcessBundle\Modules\Comparer\Services
 */
class Compare
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $operators = [];

    /**
     * @var array
     */
    protected $valueProviders = [];

    /**
     * @var array
     */
    protected $functions = [];

    /**
     * Compare constructor.
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @param $operatorName
     * @param $id
     */
    public function addOperator($operatorName,$id)
    {
        $this->operators[$operatorName::getSign()] = $id;
    }

    /**
     * @return array
     */
    public function getOperatorSigns()
    {
        return array_keys($this->operators);
    }

    /**
     * @param $sign
     * @return mixed
     */
    public function getOperatorBySign($sign)
    {
        if (array_key_exists($sign,$this->operators)) {
            return $this->container->get($this->operators[$sign]);
        }
        throw new \Exception(sprintf('Operator for sign "%s" not found.',$sign));
    }

    /**
     * @param $functionName
     * @param $id
     */
    public function addFunction($functionName,$id)
    {
        $this->functions[$functionName::getName()] = $id;
    }

    /**
     * @return array
     */
    public function getFunctionNames()
    {
        return array_keys($this->functions);
    }

    /**
     * @param $sign
     * @return mixed
     */
    public function getFunctionByName($name)
    {
        if (array_key_exists($name,$this->functions)) {
            return $this->container->get($this->functions[$name]);
        }
        throw new \Exception(sprintf('Function with name "%s" not found.',$name));
    }

    /**
     * @param $className
     * @param $id
     */
    public function addValueProvider($className, $id)
    {
        $this->valueProviders[$className::getName()] = $id;
    }

    /**
     * @return array
     */
    public function getValueProviderNames()
    {
        return array_keys($this->valueProviders);
    }

    /**
     * @param $name
     * @param null $value
     * @return IValue
     * @throws \Exception
     */
    public function getValueProviderByName($name,$value = null)
    {
        if (array_key_exists($name,$this->valueProviders)) {
            /** @var IValue $instance */
            $instance = $this->container->get($this->valueProviders[$name]);
            $instance->setValue($value);
            return $instance;
        }
        throw new \Exception(sprintf('Valueprovider with name "%s" not found.',$name));
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getAllValueProviderTerms()
    {
        $result = [];
        foreach ($this->valueProviders as $key=>$provider) {
            /** @var IValue $valueProvider */
            $valueProvider = $this->getValueProviderByName($key);
            $result[$key] = $valueProvider->getExistingValues($this->container);
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getOperators()
    {
        return $this->operators;
    }

    /**
     * @return array
     */
    public function getValueProviders()
    {
        return $this->valueProviders;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return $this->functions;
    }

    /**
     * @param IValue $left
     * @param IOperator $operator
     * @param IValue $right
     * @return bool
     */
    public function getResult(IFunction $leftFunction, IValue $leftValue, IOperator $operator, IFunction $rightFunction,IValue $rightValue)
    {
        $leftValue = $leftFunction::execute($leftValue);
        $rightValue = $rightFunction::execute($rightValue);

        return $operator::resolve($leftValue,$rightValue);
    }
}
