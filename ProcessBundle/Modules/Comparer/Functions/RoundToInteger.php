<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Functions;

use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IFunction;
use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IValue;

/**
 * Class None
 * @package Missbach\ProcessBundle\Modules\Comparer\Functions
 */
class RoundToInteger implements IFunction
{
    /**
     * @return string
     */
    public static function getName()
    {
        return 'RoundToInteger';
    }

    /**
     * @param IValue $value
     * @return int|IValue
     */
    public static function execute(IValue $value)
    {
        return round($value->transformValue());
    }
}
