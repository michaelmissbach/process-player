<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Functions;

use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IFunction;
use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IValue;

/**
 * Class None
 * @package Missbach\ProcessBundle\Modules\Comparer\Functions
 */
class None implements IFunction
{
    /**
     * @return string
     */
    public static function getName()
    {
        return 'None';
    }

    /**
     * @param IValue $value
     * @return IValue|mixed
     */
    public static function execute(IValue $value)
    {
        return $value->transformValue();
    }
}
