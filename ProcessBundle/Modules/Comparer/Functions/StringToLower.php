<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Functions;

use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IFunction;
use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IValue;

/**
 * Class None
 * @package Missbach\ProcessBundle\Modules\Comparer\Functions
 */
class StringToLower implements IFunction
{
    /**
     * @return string
     */
    public static function getName()
    {
        return 'StringToLower';
    }

    /**
     * @param IValue $value
     * @return int|IValue
     */
    public static function execute(IValue $value)
    {
        return strtolower($value->transformValue());
    }
}
