<?php

namespace Missbach\ProcessBundle\Modules\Comparer\Functions;

use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IFunction;
use Missbach\ProcessBundle\Modules\Comparer\Interfaces\IValue;

/**
 * Class None
 * @package Missbach\ProcessBundle\Modules\Comparer\Functions
 */
class StringToUpper implements IFunction
{
    /**
     * @return string
     */
    public static function getName()
    {
        return 'StringToUpper';
    }

    /**
     * @param IValue $value
     * @return int|IValue
     */
    public static function execute(IValue $value)
    {
        return strtoupper($value->transformValue());
    }
}
