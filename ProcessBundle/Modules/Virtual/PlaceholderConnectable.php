<?php

namespace Missbach\ProcessBundle\Modules\Virtual;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IConstructModifyable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;
use Missbach\ProcessBundle\Modules\Chain\Services\ChainService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PlaceholderConnectable implements IConnectable, IConstructModifyable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 5;
    const __OUTPUTS = 5;
    const __CATEGORY = 'Dummy';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * VirtualConnectable constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        if ($container->has('process.editor.service')) {
            $this->editorService = $container->get('process.editor.service');
        }
    }

    /**
     * @param IConnectable $instance
     * @param $id
     * @param $processMetas
     * @return mixed|void
     */
    public function __onConstruct(IConnectable $instance,&$id, &$processMetas)
    {
        $config = $processMetas['config'];
        $wrappedInstance = array_key_exists('___placeholder_name',$config) ? $config['___placeholder_name'] : null;
        if($wrappedInstance && $this->container->has($wrappedInstance)) {
            return $this->container->get($wrappedInstance);
        }
    }

    /**
     * @param IConnectable $instance
     * @return mixed|void
     */
    public function __onDestruct(IConnectable $instance)
    {
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        return true;
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        return true;
    }

    /**
     * @param $dryMode
     */
    public function __execute($dryMode)
    {
    }

    /**
     * @return string
     */
    public function __getEditor()
    {
        $objectInformations = $this->__getProcessMeta('objectInformations');
        return $this->editorService->render(
            '@VisualProcessModule/Virtual/Resources/views/Placeholder/editor.html.twig',
            [
                'config'=>$this->__getConfig()
            ]
        );
    }
}
