<?php

namespace Missbach\ProcessBundle\Modules\Virtual;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;
use Missbach\ProcessBundle\Modules\Chain\Services\ChainService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NotfoundConnectable implements IConnectable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 5;
    const __OUTPUTS = 5;
    const __CATEGORY = '';

    /**
     * VirtualConnectable constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        if ($container->has('process.editor.service')) {
            $this->editorService = $container->get('process.editor.service');
        }
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        return true;
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        return true;
    }

    /**
     * @param $dryMode
     */
    public function __execute($dryMode)
    {
    }

    /**
     * @return string
     */
    public function __getEditor()
    {
        $objectInformations = $this->__getProcessMeta('objectInformations');
        return $this->editorService->render(
            '@VisualProcessModule/Virtual/Resources/views/Notfound/editor.html.twig',
            [
                'name'=>$objectInformations['service_id'],
                'config'=>$this->__getConfig()
            ]
        );
    }
}
