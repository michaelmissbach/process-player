<?php

namespace Missbach\ProcessBundle\Objects;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IUnResolvable;
use Missbach\ProcessBundle\Core\Traits\UnResolveableTrait;

/**
 * Class MapConnectable
 * @package Missbach\ProcessBundle\Objects
 */
class MapConnectable implements IConnectable,IUnResolvable
{
    use UnResolveableTrait;

    const __CATEGORY = 'Test';
    const __NAME = 'Map';
}
