<?php

namespace Missbach\ProcessBundle\Objects;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;

/**
 * Class OrConnectable
 * @package Missbach\ProcessBundle\Objects
 */
class OrConnectable implements IConnectable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 2;
    const __CATEGORY = 'Connector';

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        $results = $this->__getRawResults();
        return isset($results[1]) || isset($results[2]);
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        $results = $this->__getRawResults();
        return (isset($results[1]) && $results[1]) || (isset($results[2]) && $results[2]);
    }
}
