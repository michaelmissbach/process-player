<?php

namespace Missbach\ProcessBundle\Objects;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;

/**
 * Class AndConnectable
 * @package Missbach\ProcessBundle\Objects
 */
class XOrConnectable implements IConnectable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 2;
    const __CATEGORY = 'Connector';

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        $results = $this->__getRawResults();
        return (isset($results[1]) ? (bool)$results[1] : false) || (isset($results[2]) ? (bool)$results[2] : false);
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        $results = $this->__getRawResults();
        return (isset($results[1]) ? (bool)$results[1] : false) xor (isset($results[2]) ? (bool)$results[2] : false);
    }
}
