<?php

namespace Missbach\ProcessBundle\Objects;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;

/**
 * Class AndConnectable
 * @package Missbach\ProcessBundle\Objects
 */
class DummyFalseConnectable implements IConnectable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 0;
    const __OUTPUTS = 1;
    const __CATEGORY = 'Dummy';
    const __NAME = 'Dummy-false';

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        return true;
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        return false;
    }
}
