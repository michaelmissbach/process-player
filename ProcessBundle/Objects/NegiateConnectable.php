<?php

namespace Missbach\ProcessBundle\Objects;

use Missbach\ProcessBundle\Core\Interfaces\IConnectable;
use Missbach\ProcessBundle\Core\Interfaces\IResolvable;
use Missbach\ProcessBundle\Core\Traits\ResolveableTrait;

/**
 * Class AndConnectable
 * @package Missbach\ProcessBundle\Objects
 */
class NegiateConnectable implements IConnectable,IResolvable
{
    use ResolveableTrait;

    const __INPUTS = 1;
    const __CATEGORY = 'Connector';

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __isResolved($dryMode)
    {
        $results = $this->__getRawResults();
        return isset($results[1]);
    }

    /**
     * @param bool $dryMode
     * @return bool
     */
    public function __getResult($dryMode)
    {
        $results = $this->__getRawResults();
        if (!isset($results[1])) {
            return true;
        }
        return !$results[1];
    }

    /**
     * @return bool|void
     */
    public function __forceContinueProcess()
    {
        return true;
    }
}
