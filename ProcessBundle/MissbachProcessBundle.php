<?php

namespace Missbach\ProcessBundle;

use Missbach\ProcessBundle\Core\Tools\StringHelper;
use Missbach\ProcessBundle\DependencyInjection\RegisterPass;
use Missbach\ProcessBundle\DependencyInjection\TwigThemePass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;

/**
 * Class MissbachProcessBundle
 * @package Missbach\ProcessBundle
 */
class MissbachProcessBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new RegisterPass(),PassConfig::TYPE_AFTER_REMOVING);

        $this->addModuleViews($container);

        $this->readAndExecutesPasses($container);
    }

    /**
     * @param $container
     */
    protected function addModuleViews($container)
    {
        $pathes = [realpath(sprintf('%s/Modules',dirname(__FILE__)))];
        $container->addCompilerPass(new TwigThemePass($pathes),PassConfig::TYPE_OPTIMIZE);
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function readAndExecutesPasses(ContainerBuilder $container)
    {
        $it = new \RecursiveDirectoryIterator(sprintf('%s/Modules',dirname(__FILE__)));
        foreach(new \RecursiveIteratorIterator($it) as $file)
        {
            $fileName = $file->getFilename();
            if (in_array($fileName, ['.','..',''])) {
                continue;
            }
            if (StringHelper::endsWith($fileName,'Pass.php')) {

                $namespace = $this->getFullNamespace($file->getPathname());
                $className = str_replace('.php','',$fileName);
                $fullClassName = sprintf('%s\\%s',$namespace,$className);
                if (class_exists($fullClassName)) {
                    $container->addCompilerPass(new $fullClassName(),PassConfig::TYPE_AFTER_REMOVING);
                }
            }
        }
    }

    /**
     * @param $filename
     * @return mixed
     */
    protected function getFullNamespace($filename)
    {
        $lines = file($filename);
        $result = preg_grep('/^namespace /', $lines);
        $namespaceLine = array_shift($result);
        $namespaceLine = trim(preg_replace('/\s\s+/', ' ', $namespaceLine));
        $match = array();
        preg_match('/^namespace (.*);$/', $namespaceLine, $match);
        $fullNamespace = array_pop($match);

        return $fullNamespace;
    }
}
