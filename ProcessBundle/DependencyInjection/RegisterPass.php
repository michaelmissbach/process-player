<?php

namespace Missbach\ProcessBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Class RegisterPass
 * @package Missbach\ProcessBundle\DependencyInjection
 */
class RegisterPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $this->registerObjects($container);
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function registerObjects(ContainerBuilder $container)
    {
        $definition = $container->findDefinition('process.core.factory');
        $taggedServices = $container->findTaggedServiceIds('process.object');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $tag) {
                $def = $this->convertDefinitionToArray($container->findDefinition($id));
                $definition->addMethodCall('add', array($id,$def));
            }
        }
    }

    /**
     * @param Definition $definition
     * @throws \ReflectionException
     */
    protected function convertDefinitionToArray(Definition $definition)
    {
        $informations = [];

        $newDefinition = clone $definition;

        $reflect = new \ReflectionClass($newDefinition);
        $props   = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PRIVATE);

        foreach ($props as $prop) {
            $name = $prop->getName();
            $method = sprintf('get%s',ucfirst($name));
            if (method_exists($newDefinition,$method)) {
                $informations[$name] = $newDefinition->$method();
            }
        }

        return $informations;
    }
}
