<?php

namespace Missbach\ProcessBundle\DependencyInjection;

use Missbach\ProcessBundle\Loader\RouteLoader;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class MissbachProcessExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');

        $this->readModulesConfigurations($container);

    }

    /**
     * @param ContainerBuilder $container
     */
    protected function readModulesConfigurations(ContainerBuilder $container)
    {
        $it = new \RecursiveDirectoryIterator(sprintf('%s/../Modules',dirname(__FILE__)));
        foreach(new \RecursiveIteratorIterator($it) as $file)
        {
            $fileName = $file->getFilename();
            if (in_array($fileName, ['.','..',''])) {
                continue;
            }

            if ($fileName === 'modules.yaml') {
                $loader = new Loader\YamlFileLoader($container, new FileLocator(realpath($file->getPath())));
                $loader->load('modules.yaml');
            }
        }
    }
}
