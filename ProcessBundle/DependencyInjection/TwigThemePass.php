<?php

namespace Missbach\ProcessBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class TwigThemePass
 */
class TwigThemePass implements CompilerPassInterface
{
    /**
     * @var array
     */
    protected $pathes = [];

    /**
     * TwigThemePass constructor.
     * @param $pathes
     */
    public function __construct($pathes)
    {
        $this->pathes = $pathes;
    }

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('twig.loader.native_filesystem')) {
            return;
        }

        $loader = $container->getDefinition('twig.loader.native_filesystem');

        foreach ($this->pathes as $key=>$path) {
            if (is_dir($path)) {
                $loader->addMethodCall('addPath', [
                    $path,
                    'VisualProcessModule'
                ]);
            }
        }
    }
}
