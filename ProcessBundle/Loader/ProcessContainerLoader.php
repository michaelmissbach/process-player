<?php

namespace Missbach\ProcessBundle\Loader;

use Missbach\ProcessBundle\Core\Collections\ProcessCollection;
use Missbach\ProcessBundle\Core\Exceptions\NotImplementedException;
use Missbach\ProcessBundle\Core\Interfaces\IProcessContainerLoader;
use Missbach\ProcessBundle\Core\ProcessStructureTree\Element;
use Missbach\ProcessBundle\Services\ProcessService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProcessContainerLoader
 * @package Missbach\ProcessBundle\Loader
 */
class ProcessContainerLoader implements IProcessContainerLoader
{
    /**
     * @var Element
     */
    protected $root;
    
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ProcessContainerLoader constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return sprintf('%s/process.dat',$this->container->get('kernel')->getRootDir());
    }

    /**
     * @return void
     */
    public function load()
    {
        try {
            if (file_exists($this->getFileName())) {
                $content = file_get_contents($this->getFileName());
                $content = unserialize($content);

                //@todo: check major and minor version!
                $this->root = $content['data'];

            }
        } catch (\Exception $e) {
        }

        if (!($this->root instanceof Element)) {
            $this->root = new Element();
            $this->root->setShownName('Visual Processes');
            try {
                $this->save($this->root);
            } catch (\Exception $e) {
            }

        }
    }

    /**
     * @return void
     */
    public function save(Element $root)
    {
        $saveInformations = [
            'major_v'=>ProcessService::VERSION_MAJOR,
            'minor_v'=>ProcessService::VERSION_MINOR,
            'rev_v'=>ProcessService::VERSION_REV,
            'data'=>$root
        ];
        file_put_contents($this->getFileName(),serialize($saveInformations));
    }

    /**
     * @return mixed
     */
    public function getTreeStructure()
    {
        return $this->root;
    }

    /**
     * @return array
     */
    public function getFlat()
    {
        return $this->flattensTreeConstructed();
    }

    /**
     * @return array
     */
    public function getListWithMetas()
    {
        return $this->flattensTree();
    }

    /**
     * @param $id
     * @return ???
     */
    public function getProcessById($id)
    {
        $this->flattensTree(null,$return);
        return $return[$id];
    }

    /**
     * @param $id
     * @param ??? $process
     * @return mixed
     */
    public function setProcessById($id, $process)
    {
        throw new NotImplementedException();
    }

    /**
     * @param null $node
     * @param array $return
     * @return array
     */
    protected function flattensTreeConstructed($node = null,&$return = [])
    {
        if (!$node) {
            $node = $this->root;
        }

        $collection = new ProcessCollection();
        if ($node->getData()) {
            foreach ($node->getData() as $key=>$value) {
                $collection->addItem($key,$value);
            }
        }
        $return[$node->getId()] = $collection;

        if ($node->hasChildren()) {
            /**
             * @var  $key
             * @var Element $element
             */
            foreach ($node->getChildren() as $key=>$element) {
                $this->flattensTreeConstructed($element,$return);
            }
        }

        return $return;
    }

    /**
     * @param null $node
     * @param array $return
     * @return array
     */
    protected function flattensTree($node = null,&$return = [])
    {
        if (!$node) {
            $node = $this->root;
        }

        if (!$node->isRoot()) {
            $return[$node->getId()] = [
                'id' => $node->getId(),
                'shownName' => $node->getShownName(),
                'callName' => $node->getCallName(),
            ];
        }

        if ($node->hasChildren()) {
            /**
             * @var  $key
             * @var Element $element
             */
            foreach ($node->getChildren() as $key=>$element) {
                $this->flattensTree($element,$return);
            }
        }

        return $return;
    }

}
